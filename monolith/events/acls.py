from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json, requests

def picture_request(city,state):
    headers = {"AUTHORIZATION":PEXELS_API_KEY}
    search_url = f"https://api.pexels.com/v1/search?query={city}, {state}&per_page=1"
    response = requests.get(search_url,headers=headers).json()
    pic_url = response["photos"][0]["url"]
    return pic_url

def weather_request(city, state):
    country = 'USA'
    GC_search_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{country}&appid={OPEN_WEATHER_API_KEY}"
    print (GC_search_url)
    GC_response = requests.get(GC_search_url).json()
    lat,lon = GC_response[0]['lat'],GC_response[0]['lon']
    weather_search_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather_search_url).json()
    weather = {
        "description":weather_response['weather'][0]['description'],
        "temperature":weather_response['main']['feels_like']
        }
    return weather
