from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, datetime):
            return object.isoformat()
        else:
            return super().default(object)

class QuerySetEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, QuerySet):
            return list(object)
        else:
            return super().default(object)

class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}   
    def default(self, object):
        if isinstance(object, self.model):  # if the object to decode is the same class as what's in the model property, then
            newdict = {}# create an empty dictionary to unload keys(property names) and values into
            if hasattr(object, "get_api_url"):
                    newdict["href"] = object.get_api_url()
            
            for pname  in self.properties:  # for each propery name in the properties list(provided by the encoder class we create on the view page)
                value = getattr(object, pname)  # get the corresponding property value from the model object
                if pname in self.encoders:
                    encoder = self.encoders[pname]
                    value = encoder.default(value)
                newdict[pname] = value  # put the property name : property value pair into the dictionary
                newdict.update(self.get_extra_data(object))
            return newdict  # return the dictionary
        else:
            # From the documentation
            return super().default(object)
            
    def get_extra_data(self,object):
        return {}
